import {Component, Input} from '@angular/core';
import {Film} from '../shared/film.model';
import {FilmService} from '../shared/film.service';

@Component({
  selector: 'app-film-item',
  templateUrl: './film-item.component.html',
  styleUrls: ['./film-item.component.css']
})
export class FilmItemComponent{
  @Input() film!: Film;

  constructor(private filmService: FilmService) {}

  onDelete() {
    this.filmService.deleteFilm(this.film.id).subscribe(() => {
      this.filmService.fetchFilms();
    });
  }
}
