import {Component, OnDestroy, OnInit} from '@angular/core';
import {FilmService} from '../shared/film.service';
import {Film} from '../shared/film.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.css']
})
export class AddListComponent implements OnInit, OnDestroy {
  name = '';

  isUpLoading = false;
  filmUploadingSubscription!: Subscription;

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.filmUploadingSubscription = this.filmService.filmUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });
  }

  addFilmToList() {
    const name: string = this.name;
    const id = Math.random().toString();
    const film = new Film(id, name);

    this.filmService.addFilm(film).subscribe(() => {
      this.filmService.fetchFilms();
    });
  }

  ngOnDestroy(): void {
    this.filmUploadingSubscription.unsubscribe();
  }

}
