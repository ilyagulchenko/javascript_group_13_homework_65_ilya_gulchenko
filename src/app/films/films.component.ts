import {Component, OnDestroy, OnInit} from '@angular/core';
import {Film} from '../shared/film.model';
import {FilmService} from '../shared/film.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit, OnDestroy {
  films!: Film[];
  filmsChangeSubscriptions!: Subscription;
  filmsFetchingSubscriptions!: Subscription;
  isFetching: boolean = false;

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.filmsChangeSubscriptions = this.filmService.filmsChange.subscribe((films: Film[]) => {
      this.films = films;
    });
    this.filmsFetchingSubscriptions = this.filmService.filmsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.filmService.fetchFilms();
  }

  ngOnDestroy(): void {
    this.filmsChangeSubscriptions.unsubscribe();
    this.filmsFetchingSubscriptions.unsubscribe();
  }

}
