import {HttpClient} from '@angular/common/http';
import {Film} from './film.model';
import {map, Subject, tap} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()

export class FilmService {
  filmsChange = new Subject<Film[]>();
  filmsFetching = new Subject<boolean>();
  filmUpLoading = new Subject<boolean>();

  private films: Film[] = [];

  constructor(private http: HttpClient) {}

  addFilm(film: Film) {
    const body = {
      name: film.name,
    };

    this.filmUpLoading.next(true);

    return this.http.post('https://plovo-cc061-default-rtdb.firebaseio.com/films.json', body).pipe(
      tap(() => {
        this.filmUpLoading.next(false);
      })
    );
  }

  fetchFilms() {
    this.filmsFetching.next(true);
    this.http.get<{[id: string]: Film}>('https://plovo-cc061-default-rtdb.firebaseio.com/films.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const filmData = result[id];

          return new Film(
            id,
            filmData.name,
          );
        });
      }))
      .subscribe(films => {
        this.films = films;
        this.filmsChange.next(this.films.slice());
        this.filmsFetching.next(false);
      });
  }

  deleteFilm(id: string) {
    return this.http.delete(`https://plovo-cc061-default-rtdb.firebaseio.com/films/${id}.json`);
  }
}
